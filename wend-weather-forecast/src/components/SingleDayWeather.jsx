import React from 'react'

const SingleDayWeather = (props) => {
    
        return  (<div className="weatherOne">
            <br/>
                    <p><span className="day">DAY---{props.index} </span></p>
                    <p><span>Location: </span>{props.city} {props.country}</p>
                    <p> <span>Temperature:</span>{props.temperature}</p>
                    <p><span>Humidity: </span>{props.humidity}</p>
                    <p><span>description:</span> {props.description}</p>
            <br />
                </div>)
        
    
}

export default SingleDayWeather;