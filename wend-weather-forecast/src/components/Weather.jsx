import React from 'react'
import SingleDayWeather from './SingleDayWeather.jsx'


const Weather = (props) => {

    
        return props.temperature.map((temp, index) => {
            return <SingleDayWeather className = "weatherOne" key={index} index={index} temperature={temp} country = {props.country} city={props.city} humidity={props.humidity[index]} description={props.description[index]}/>
            });
        
        
    
}



export default Weather;