import React from 'react';

const Form = (props) =>{

        return(
            <form onSubmit={props.getWeather}>
                <input type="text" name="city" placeholder="Your city"/>
                <input type="text" name="country" placeholder="Your country"/>
                <button>Get Weather</button>
            </form>
        )
}


export default Form;