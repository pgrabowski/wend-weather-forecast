import React, { Component } from 'react';
import Titles from './components/Titles.jsx'
import Form from './components/Form.jsx'
import Weather from './components/Weather.jsx'
const API_KEY = '48c659843709af18e4546e77a8857dca';





class App extends Component {

  state = {
    city: undefined,
    country: undefined,
    temperature: [],
    humidity: [],
    error: undefined,
    description: [],
    isLoc: false
  }

  setWeather = async(city, country) => {
    const api_call =  await fetch(`https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/forecast?q=${city},${country}&appid=${API_KEY}&units=metric`);
    console.log(api_call);
    const data = await api_call.json();
    console.log(data);
    this.setState({
      temperature:  [data.list[0].main.temp,data.list[8].main.temp,data.list[16].main.temp,data.list[24].main.temp,data.list[32].main.temp],
      city: data.city.name,
      country:data.city.country,
      humidity: [data.list[0].main.humidity,data.list[8].main.humidity,data.list[16].main.humidity,data.list[24].main.humidity,data.list[32].main.humidity],
      description: [data.list[0].weather[0].description,data.list[8].weather[0].description,data.list[16].weather[0].description,data.list[24].weather[0].description,data.list[32].weather[0].description],
      error: undefined,
      isLoc: false
    })
  }
    getLocation = async () => {

    let setWeather2 = async(obj) => {
      console.dir(obj)
      const api_call =  await fetch(`https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/forecast?lat=${obj.coords.latitude}&lon=${obj.coords.longitude}&appid=${API_KEY}&units=metric`);
      console.log(api_call);
      const data = await api_call.json();
      console.log(data);
      this.setState({
        temperature:  [data.list[0].main.temp,data.list[8].main.temp,data.list[16].main.temp,data.list[24].main.temp,data.list[32].main.temp],
        city: data.city.name,
        country:data.city.country,
        humidity: [data.list[0].main.humidity,data.list[8].main.humidity,data.list[16].main.humidity,data.list[24].main.humidity,data.list[32].main.humidity],
        description: [data.list[0].weather[0].description,data.list[8].weather[0].description,data.list[16].weather[0].description,data.list[24].weather[0].description,data.list[32].weather[0].description],
        error: undefined,
        isLoc: false
      })
    }
  
    navigator.geolocation.getCurrentPosition(setWeather2)
  
    //const data1 = await fetch('http://ip-api.com/json');
    //const dataJson = await data1.json();
  
  }



  fetchWeather() {
    this.getLocation().then(
        console.log('done')
    );
  }

  componentDidMount() {
    this.fetchWeather();
}

getWeather = (event) => {
  event.preventDefault();
  const city=event.target.elements.city.value ;
  const country=event.target.elements.country.value;
  if(city && country)
    this.setWeather(city, country);
}

  render() {
    return (
        <div>
          <div className="wrapper">
            <div className="main">
              <div className="container">
                <div className="row">

                    <div className="col-xs-7 form-container">
                      <Form getWeather={this.getWeather} styleName="getWeather"/>
                      <div className = "weatherGrid">
                      <Weather temperature = {this.state.temperature}
                      city={this.state.city}
                      country={this.state.country}
                      humidity={this.state.humidity}
                      description={this.state.description}
                      error={this.state.error}
                      />
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        }
      };
      
      export default App;


